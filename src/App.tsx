import React from 'react';
import './App.css';
import TablePage from './lib/Workspace/page/TablePage';

function App() {
  return (
    <div className="App">
      <TablePage rowId={2} />
    </div>
  );
  
}

export default App;
