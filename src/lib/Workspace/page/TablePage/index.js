import "antd/dist/antd.css";
import React, { useState } from "react";
import { Button } from "antd";
import GenericModal from "../../sharedComponents/GenericModal";
import GenericTable from "../../sharedComponents/GenericTable";
import { fetchFeatures, fetchModelMetadata, columnsNames } from "./services";

export default function TablePage({ rowId }) {
  const [modalVisibility, setModalVisibility] = useState(false);
  const [columnsForTable, setColumnsForTable] = useState(null);
  const [dataforTable, setDataForTable] = useState(null);

  const columnPromise = new Promise((resolve, reject) => {
    let columns = columnsNames();
    if (columns) {
      resolve(columns);
    } else {
      reject("Promise is rejected");
    }
  });
  columnPromise.then((res) => {
    if (columnsForTable === null) {
      let columnsToAdd = [];
      res.forEach((elem) => {
          
        columnsToAdd.push({ title: elem, dataIndex: elem,  sorter: {
            compare: (a, b) => a.elem - b.elem,
        
          }, });
      });
      setColumnsForTable(columnsToAdd);
    }
  });

  const dataPromise = new Promise((resolve, reject) => {
    let data = fetchFeatures();
    if (data) {
      resolve(data);
    } else {
      reject("Promise is rejected");
    }
  });
  dataPromise.then((res) => {
    if (dataforTable === null) {
      let dataToAdd = [];
      res.forEach((res) => {
        dataToAdd.push({
          key: res.id,
          name: res.attributes.name,
          value: res.attributes.value,
          varId: res.attributes.varId,
          type: res.attributes.type,
          description: res.attributes.description,
          labels: res.attributes.labels,
          domains: res.attributes.domains,
        });
      });
      setDataForTable(dataToAdd);
    }
  });


  return (
    <div>
      <Button onClick={(e) => setModalVisibility(true)} type="primary">
        Features Table
      </Button>

      <GenericModal
        modalVisibleState={modalVisibility}
        modalTitle={"Available Feaures"}
        modalContent={<GenericTable tableColumns={columnsForTable} tableData={dataforTable}></GenericTable>}
        width={1000}
        onCancel={(e) => setModalVisibility(false)}
        okText={"Save"}
        handleModalChange={() => {}}
      />
    </div>
  );
}
