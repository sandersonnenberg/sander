import delay from 'delay';
import { features, modelMetadata } from './data';
import {INodeAttributes} from './types';
export async function fetchFeatures() {
  await delay(1500);

  return features;
}

export async function columnsNames() {
    await delay(1500);
  
    return INodeAttributes;
  }

export async function fetchModelMetadata(){
    await delay(1500);
  
    return modelMetadata;
  }
  
