export const NodeTypes={
    METRIC : 'METRIC', 
    ALEX : 'ALEX', 
    SUBMETRIC : 'SUBMETRIC', 
    FEATURE : 'FEATURE',
    COMPUTATIONAL_TEMP : 'COMPUTATIONAL_TEMP'
}


export const INodeAttributes =['name','varId','type','labels','domains','description',
    'value'
]

export const ICrNode= {
    id:'id',
    attributes:'attributes'
}

export const IModelMetadata= {
    labels:'labels',
    domains:'domains'
}