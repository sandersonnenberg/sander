import React, { useState, Fragment, useEffect } from 'react';
import { Modal } from 'antd';
import PropTypes from 'prop-types';

export default function GenericModal({
  modalVisibleState,
  modalTitle,
  modalContent,
  onCancel,
  okText,
  width}) {
  const [visible, setVisible] = useState(modalVisibleState);
  const [modaldata, updateModalData] = useState(null);

  useEffect(() => {
    setVisible(modalVisibleState);
  }, [modalVisibleState]);

  return (
    <div>
      <Modal
        title={modalTitle}
        visible={visible}
        onCancel={handleCancel}
        width={width}
        okText={okText}
      >
        <Fragment>
          {React.cloneElement(modalContent, { handleModalChange })}
        </Fragment>
      </Modal>
    </div>
  );

  function handleModalChange(updatedData) {
    updateModalData(updatedData);
  }

  function handleCancel(data) {
    setVisible(false);
    onCancel(data);
  }
}

GenericModal.propTypes = {
  /** modalVisibleState - indicate if the modal is visible or not */
  modalVisibleState: PropTypes.bool,
  /** modalTitle - title of the modal */
  modalTitle: PropTypes.string,
  /** modalContent - the component that will be shown inside the modal */
  modalContent: PropTypes.element.isRequired,
  /** onCancel - function activated when the modal gets closed with cancel */
  onCancel: PropTypes.func.isRequired,
  /** okText - text that will be shown on the ok button */
  okText: PropTypes.string,
  /** width - the width of the modal */
  width: PropTypes.number,

  handleModalChange: PropTypes.func
};

GenericModal.defaultProps = {
  modalVisibleState: false,
  modalTitle: '',
  width: 520,
  okText: 'OK',
};
