import React, { useState } from "react";
import { Table, Divider, Button } from "antd";

export default function GenericTable({ tableColumns, tableData }) {
  const [columns, setColumns] = useState(tableColumns);
  const [data, setData] = useState(tableData);
  
  const [countRowsSelected, setCountRowsSelected] = useState(0);
  const [rowsSelected, setRowsSelected] = useState(0);




  
  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      console.log(
        `selectedRowKeys: ${selectedRowKeys}`,
        "selectedRows: ",
        selectedRows
      );
      setCountRowsSelected(selectedRows.length);
      setRowsSelected(selectedRows);
    },
  };


  const [searchText,setSearchText]= useState('')
  const[searchedColumn,searchSearchedColumn] = useState('');


  
  const [selectionType, setSelectionType] = useState("checkbox");
  const hasSelected = countRowsSelected > 0;
  return (
    <div>
      <div style={{ marginBottom: 16 }}>
        <Button
          type="primary"
          onClick={(e) => handleDelete()}
          disabled={!hasSelected}
          danger
        >
          Delete
        </Button>
        <span style={{ marginLeft: 8 }}></span>
      </div>
      <Divider />

      <Table
        rowSelection={{
          type: selectionType,
          ...rowSelection,
        }}
        columns={columns}
        dataSource={data}
      />
    </div>
  );

  function handleDelete() {
    const tempData = data;
    rowsSelected.forEach((element) => {
      var pos = data.indexOf(element);
      tempData.splice(pos, 1);
      debugger;
      setData(tempData);
    });
  }




  


}
